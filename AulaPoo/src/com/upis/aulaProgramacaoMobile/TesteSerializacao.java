package com.upis.aulaProgramacaoMobile;


import java.io.*;

public class TesteSerializacao {
    public static void main(String[] args) {
        Horario horario = new Horario((byte)10,(byte)12,(byte)30);

        try {
            System.out.println("Serialização");
            FileOutputStream fileOut = new FileOutputStream("horario.ser");
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(horario);
            out.close();
            fileOut.close();

            System.out.println("Desserialização");
            FileInputStream fileIn = new FileInputStream("horario.ser");
            ObjectInputStream in = new ObjectInputStream(fileIn);
            Horario horarioDesserializado = (Horario) in.readObject();
            in.close();
            fileIn.close();

            System.out.println("Hora: " + horarioDesserializado.getHora() + ", Minuto: " + horarioDesserializado.getMinuto());
        } catch (IOException | ClassNotFoundException i) {
            i.printStackTrace();
        }
    }
}
