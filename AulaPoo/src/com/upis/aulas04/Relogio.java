package com.upis.aulas04;

public class Relogio {

	private Horario hms;
	private Data dma;
	
	public Relogio(Horario hms, Data dma) {
		this.hms = new Horario(hms);
		this.dma = new Data(dma);
	}
	
	public void tictac() {
		
		hms.incrementaSegundo();

		if(hms.ehPrimeiroHorario()) {
			dma.incrementaDia();
		}		
	}
	
	@Override
	public String toString() {
		return dma + " " + hms;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null || getClass() != obj.getClass())
			return false;
	
		Relogio other = (Relogio) obj;
		if (dma == null) {
			if (other.dma != null)
				return false;
		} else if (!dma.equals(other.dma))
			return false;
		if (hms == null) {
			if (other.hms != null)
				return false;
		} else if (!hms.equals(other.hms))
			return false;
		return true;
	}
	

	
	
}
