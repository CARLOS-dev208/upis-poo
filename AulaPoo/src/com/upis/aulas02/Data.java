package com.upis.aulas02;

public class Data {
	
	private byte dia; 	// {1 .. {28, 29, 30 ou 31} }
	private byte mes; 	// {1 .. 12}
	private short ano; 	// {1 .. 9999}

	private boolean ehBissexto(short ano) {
		return (ano % 400 == 0) || ((ano % 4 == 0) && (ano % 100 != 0));
	}

	private byte getUltimoDia(byte mes, short ano) {
		byte ud [] = {0,31,28,31,30,31,30,31,31,30,31,30,31}; 
		
		if(mes == 2 && ehBissexto(ano)) {
			return 29;
		}
		
		return ud[mes];
	}
	
	public Data() {
		setAno((byte)1);
		setMes((byte)1);
		setDia((byte)1);
	}
	
	public Data(byte dia, byte mes, short ano) {
		this();
		setAno(ano);
		setMes(mes);
		setDia(dia);
	}
	
	public byte getDia() {
		return dia;
	}

	public void setDia(byte dia) {
		
		byte ultimoDia = getUltimoDia(mes, ano);
		
		if(dia >= 1 && dia <= ultimoDia) {
			this.dia = dia;
		}
	}

	public byte getMes() {
		return mes;
	}

	public void setMes(byte mes) {
		if(mes >= 1 && mes <= 12) {
			this.mes = mes;
		}
	}

	public short getAno() {
		return ano;
	}

	public void setAno(short ano) {
		if(ano >= 1 && ano <= 9999) {
			this.ano = ano;			
		}
	}
	
	public void incrementaDia() {
		byte incrementaEmUmDia =  (byte) (this.getDia() + 1);
		if(incrementaEmUmDia > getUltimoDia(mes, ano)) {
			setDia((byte)1);
			incrementaMes();
		}else {
			setDia(incrementaEmUmDia);
		}	
	}
	
	public void incrementaMes() {
		byte incrementaEmUmMes = (byte)(this.mes + 1);
		if(incrementaEmUmMes > 12) {
			setMes((byte)1);
			incrementaAno();
		}else {
			setMes(incrementaEmUmMes);
		}
	}
	
	public void incrementaAno() {
		short incrementaEmUmAno = (short)(this.ano + 1);
		if(incrementaEmUmAno > 9999) {
			setAno((short)1);
		}else {
			setAno(incrementaEmUmAno);	
		}
	}
	
	@Override
	public String toString() {
		return getDia() + "/" + getMes() + "/" + getAno();
	}


}
