package com.upis.aulas02;

public class Horario {

	private int representacaoDaHora; // 00:00:00
		
	
	public Horario(){
		setHora(0);
		setSegundo(0);
		setMinuto(0);
	}
	
	public Horario(byte hora, byte minuto, byte segundo){
		setHora(hora);
		setMinuto(minuto);
		setSegundo(segundo);
	}
	

	public void setHora(int hora) {
		this.representacaoDaHora += hora;
	}
	
	public int getHora() {
		int hora = (int) Math.floor(representacaoDaHora / 3600);
		if(hora >= 0 && hora <24) {
			return hora;
		}else {
			return  0;
		}
	}
	
	public void setMinuto(int minuto) {
			this.representacaoDaHora += minuto;
	}
	
	public int getMinuto() {
		int minuto = (int) Math.floor(representacaoDaHora % 3600) / 60;
		return minuto;
	}
	
	public void setSegundo(int segundo) {
			this.representacaoDaHora += segundo;
	}
	
	public int getSegundo() {
		return this.representacaoDaHora % 60;
	}
	
	public void incrementaSegundo() {
		this.representacaoDaHora += 1;
	}
	
	public void incrementaMinuto() {
		if(this.representacaoDaHora == 0) {
			this.representacaoDaHora = 59;
		}
		this.representacaoDaHora += 1;
	}
	
	public void incrementaHora() {
		if(this.representacaoDaHora == 0) {
			this.representacaoDaHora = 3600;
		}
		this.representacaoDaHora += 1;
	}
	
	

	
	@Override
	public String toString() {
		String hora = getHora() < 10 ? "0"+ getHora() : String.valueOf(getHora());
		String minuto = getMinuto() < 10 ? "0"+ getMinuto() : String.valueOf(getMinuto());
		String segundo = getSegundo() < 10 ? "0"+ getSegundo() : String.valueOf(getSegundo());
		
		return hora + ":"+ minuto + ":"+ segundo;
	}


	
	

}





